/**
 * 问题页面的评论等js
 */

$(function () {
    $("#comment-publish").click(function () {
        var url = $("#comment-form").attr('action');
        var questionId = $("#parentId").val();
        var content = $("#content").val();
        var data = {
            "parentId":questionId,
            "content":content,
            "type":1
        };
        comment(content,url,data);
    });

    $(".sub-comment-publish").click(function () {
        var url = $(this).parent(".comment-sub-form").attr('action');
        var targetId = $(this).prev().prev(".sub-parent-id").val();
        var content = $(this).prev(".sub-content").val();
        var data = {
            "parentId":targetId,
            "content":content,
            "type":2
        };
        comment(content,url,data);
    });


    $(".two-level-comment").click(function () {
        var id = this.getAttribute("data-id");
        var collapseId = $("#comment"+id);
        if(collapseId.hasClass("in")){
            $(this).removeClass("active");
            collapseId.removeClass("in");
        }else{
            $(this).addClass("active");
            collapseId.addClass("in");
            $.get("/comment/"+id,function (data) {
                var commitNode = $("#comment"+id).children("div.comment-sub").get(0);
                var html = '';
                if(data.code == 200){
                    for (key in data.data){
                        var item = data.data[key];
                        html += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 comment-list" >\n' +
                            '                                <div class="media">\n' +
                            '                                    <div class="media-left">\n' +
                            '                                        <a href="/profile/questions">\n' +
                            '                                            <img class="media-object img-rounded" src="'+item.user.avatarUrl+'"\n' +
                            '                                                 alt="'+item.user.name+'">\n' +
                            '                                        </a>\n' +
                            '                                    </div>\n' +
                            '                                    <div class="media-body">\n' +
                            '                                        <span class="media-heading">'+item.user.name+'</span>\n' +
                            '                                        <div><span >'+item.context+'</span></div>\n' +
                            '                                        <div class="menu">\n' +
                            '                                            <span class="pull-right"\n' +
                            '                                                  >'+item.gmtModified+'</span>\n' +
                            '                                        </div>\n' +
                            '                                    </div>\n' +
                            '                                </div>\n' +
                            '                            </div>';
                    }
                    $(commitNode).before(html);
                }
            });
        }
    });


    function comment(content,url,data) {
        if(!content){
            alert("评论内容不能为空");
            return;
        }
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (response) {
                console.log(response);
                if(response.code == 200){
                    location.reload();
                }
            }
        });
    }
})