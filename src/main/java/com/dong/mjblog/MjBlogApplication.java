package com.dong.mjblog;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.dong.mjblog.mapper")
public class MjBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(MjBlogApplication.class, args);
    }
}
