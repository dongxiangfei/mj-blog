package com.dong.mjblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


/**
 * @Author:dxf
 * @Date:2020/10/12-10-12 23:16
 */
@Data
@TableName("question")
public class QuestionEntity {

    /**
     * 默认使用雪花算法插入主键值
     * TableId主键
     * 定义主键策略模式
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    private String title;
    private String description;
    private Long creator;
    private Long commentCount;
    private Long viewCount;
    private Long likeCount;
    private String tag;
    private Long gmtCreate;
    private Long gmtModified;

    @TableField(exist = false)
    private UserEntity user;
}
