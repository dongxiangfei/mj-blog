package com.dong.mjblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Author:dxf
 * @Date:2020/10/5-10-05 11:16
 */
@Data
@TableName("comment")
public class CommentEntity {

    /**
     * 默认使用雪花算法插入主键值
     * TableId主键
     * 定义主键策略模式
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long parentId;
    private String context;
    private Integer type;
    private Long commentator;
    private Long likeCount;
    private Long gmtCreate;
    private Long gmtModified;
}
