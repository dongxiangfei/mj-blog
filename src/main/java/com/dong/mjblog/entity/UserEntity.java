package com.dong.mjblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Author:dxf
 * @Date:2020/10/5-10-05 11:16
 */
@Data
@TableName("user")
public class UserEntity {

    /**
     * 默认使用雪花算法插入主键值
     * TableId主键
     * 定义主键策略模式
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private String accountId;
    private String token;
    private String avatarUrl;
    private Long gmtCreate;
    private Long gmtModified;
}
