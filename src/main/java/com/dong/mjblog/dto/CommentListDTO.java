package com.dong.mjblog.dto;

import com.dong.mjblog.entity.UserEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Author:dxf
 * @Date:2020/10/4-10-04 0:10
 */
@Data
public class CommentListDTO {
    private Long id;
    private Long parentId;
    private String context;
    private Integer type;
    private Long commentator;
    private Long likeCount;
    private Long gmtCreate;
    private String gmtModified;
    private UserEntity user;
}
