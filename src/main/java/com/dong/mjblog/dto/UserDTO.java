package com.dong.mjblog.dto;

import lombok.Data;

/**
 * @Author:dxf
 * @Date:2020/10/4-10-04 0:10
 */
@Data
public class UserDTO {
    private String login;
    private Long id;
    private String avatar_url;

}
