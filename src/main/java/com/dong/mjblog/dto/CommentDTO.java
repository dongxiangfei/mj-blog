package com.dong.mjblog.dto;

import lombok.Data;

/**
 * @Author:dxf
 * @Date:2020/10/4-10-04 0:10
 */
@Data
public class CommentDTO {
    private Long parentId;
    private String content;
    private Integer type;
}
