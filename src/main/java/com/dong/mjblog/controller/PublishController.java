package com.dong.mjblog.controller;

import com.dong.mjblog.entity.QuestionEntity;
import com.dong.mjblog.entity.UserEntity;
import com.dong.mjblog.service.impl.QuestionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:dxf
 * @Date:2020/10/12-10-12 22:36
 */
@Controller
public class PublishController {

    @Autowired
    private QuestionImpl questionImpl;

    @GetMapping("/publish")
    public String publish(Model model){
        model.addAttribute("error",null);
        return "publish";
    }

    @GetMapping("/publish/{id}")
    public String edit(@PathVariable(name = "id") Integer id,Model model){
        QuestionEntity question = questionImpl.getById(id);
        model.addAttribute("question",question);
        model.addAttribute("title",question.getTitle());
        model.addAttribute("description",question.getDescription());
        model.addAttribute("tag",question.getTag());
        model.addAttribute("id",question.getId());
        return "publish";
    }

    @PostMapping("/publish")
    public String doPublish(
            @RequestParam(value = "title",required = false) String title,
            @RequestParam(value = "description",required = false) String description,
            @RequestParam(value = "tag",required = false) String tag,
            @RequestParam(value = "id",required = false) Long id,
            HttpServletRequest request,
            Model model){

        model.addAttribute("title",title);
        model.addAttribute("description",description);
        model.addAttribute("tag",tag);
        if(title == null || title == ""){
            model.addAttribute("title","标题不能为空");
            return "publish";
        }
        if(description == null || description == ""){
            model.addAttribute("description","问题补充不能为空");
            return "publish";
        }
        if(tag == null || tag == ""){
            model.addAttribute("tag","标签不能为空");
            return "publish";
        }

        Long timestamp = System.currentTimeMillis()/1000;
        UserEntity userEntity = null;
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setTitle(title);
        questionEntity.setDescription(description);
        questionEntity.setTag(tag);
        questionEntity.setGmtCreate(timestamp);
        questionEntity.setGmtModified(timestamp);
        questionEntity.setId(id);
        UserEntity user = (UserEntity)request.getSession().getAttribute("user");
        if(user == null){
            model.addAttribute("error","用户未登陆");
            return "publish";
        }
        questionEntity.setCreator(user.getId());
        boolean result = questionImpl.saveOrUpdate(questionEntity);
        return "redirect:/";
    }
}
