package com.dong.mjblog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dong.mjblog.dto.QuestionQueryDTO;
import com.dong.mjblog.entity.QuestionEntity;
import com.dong.mjblog.service.impl.QuestionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:dxf
 * @Date:2020/10/3-10-03 21:29
 */
@Controller
public class IndexController {

    @Autowired
    private QuestionImpl questionImpl;

    @GetMapping("/")
    //pn是每次传回来的当前页
    public String index(HttpServletRequest request,
                        Model model,
                        @RequestParam(required = false, defaultValue = "1", value = "pn") Integer pn,
                        @RequestParam(required = false, defaultValue = "2", value = "size") Integer size,
                        @RequestParam(required = false, defaultValue = "", value = "title") String title
                        ){
        QuestionQueryDTO questionQueryDTO = new QuestionQueryDTO();
        questionQueryDTO.setTitle(title);
        IPage<QuestionEntity> pageList = questionImpl.getPageListBySearch(questionQueryDTO,pn,size);
        request.setAttribute("jumpUrl", "/?pn=");
        model.addAttribute("pageList",pageList);
        model.addAttribute("title",title);
        return "index";
    }
}
