package com.dong.mjblog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dong.mjblog.entity.QuestionEntity;
import com.dong.mjblog.entity.UserEntity;
import com.dong.mjblog.service.impl.QuestionImpl;
import com.dong.mjblog.service.impl.UserImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author:dxf
 * @Date:2020/10/12-10-12 22:36
 */
@Controller
public class ProfileController {

    @Autowired
    private QuestionImpl questionImpl;

    @GetMapping("/profile/{action}")
    public String publish(HttpServletRequest request,
                          @PathVariable(name = "action")String action,
                          @RequestParam(required = false, defaultValue = "1", value = "pn") Integer pn,
                          Model model){
        UserEntity user = (UserEntity)request.getSession().getAttribute("user");
        if(user == null){
            return "redirect:/";
        }

        if("questions".equals(action)){
            model.addAttribute("section","questions");
            model.addAttribute("sectionName","我的提问");
            request.setAttribute("jumpUrl", "/profile/questions?pn=");
        }else if("replies".equals(action)){
            model.addAttribute("section","replies");
            model.addAttribute("sectionName","最新回复");
            request.setAttribute("jumpUrl", "/profile/replies?pn=");
        }
        IPage<QuestionEntity> pageList = questionImpl.getPageList(user.getId(),pn,2);

        model.addAttribute("pageList",pageList);
        return "profile";
    }

}
