package com.dong.mjblog.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author:dxf
 * @Date:2020/10/11-10-11 22:38
 */
public interface IBaseService<T> extends IService {
}
