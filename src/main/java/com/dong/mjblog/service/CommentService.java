package com.dong.mjblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dong.mjblog.entity.CommentEntity;

/**
 * @Author:dxf
 * @Date:2020/10/5-10-05 11:23
 */
public interface CommentService extends IService<CommentEntity> {
}
