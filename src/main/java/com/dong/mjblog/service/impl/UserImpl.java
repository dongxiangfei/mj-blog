package com.dong.mjblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dong.mjblog.entity.UserEntity;
import com.dong.mjblog.mapper.UserMapper;
import com.dong.mjblog.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @Author:dxf
 * @Date:2020/10/5-10-05 11:25
 */
@Service
public class UserImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {
}
