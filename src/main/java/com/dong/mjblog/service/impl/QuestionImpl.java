package com.dong.mjblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dong.mjblog.dto.QuestionQueryDTO;
import com.dong.mjblog.entity.QuestionEntity;
import com.dong.mjblog.entity.UserEntity;
import com.dong.mjblog.mapper.QuestionMapper;
import com.dong.mjblog.service.QuestionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author:dxf
 * @Date:2020/10/5-10-05 11:25
 */
@Service
public class QuestionImpl extends ServiceImpl<QuestionMapper, QuestionEntity> implements QuestionService{

    @Autowired
    private UserImpl userImpl;

    @Autowired
    private QuestionMapper questionMapper;

//    @Autowired
//    private QuestionMapper questionMapper;

    /**
     * 重写list方法
     * @return
     */
    public IPage<QuestionEntity> getPageList(Integer pn,Integer size){
        Page<QuestionEntity> questionPage = new Page<>(pn,size);
        QueryWrapper<QuestionEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("gmt_modified");
        IPage<QuestionEntity> questionEntityPage = page(questionPage,queryWrapper);
        hasOne(questionEntityPage);
        return questionEntityPage;
    }



    /**
     * 重写list方法
     * @return
     */
    public IPage<QuestionEntity> getPageList(Long userId,Integer pn,Integer size){
        Page<QuestionEntity> questionPage = new Page<>(pn,size);
        QueryWrapper<QuestionEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("creator",userId);
        queryWrapper.orderByDesc("gmt_modified");
        IPage<QuestionEntity> questionEntityPage = page(questionPage, queryWrapper);
        hasOne(questionEntityPage);
        return questionEntityPage;
    }


    public IPage<QuestionEntity> getPageListBySearch(QuestionQueryDTO queryDTO, Integer pn, Integer size){
        Page<QuestionEntity> questionPage = new Page<>(pn,size);
        QueryWrapper<QuestionEntity> queryWrapper = new QueryWrapper<>();

        String title = queryDTO.getTitle();
        if(title != null){
            queryWrapper.like("title","%"+ title +"%");
        }
        IPage<QuestionEntity> questionEntityPage = page(questionPage, queryWrapper);
        hasOne(questionEntityPage);
        return questionEntityPage;
    }

    /**
     * 问题关联用户
     * @param questionEntityPage
     */
    private void hasOne(IPage<QuestionEntity> questionEntityPage){
        List<QuestionEntity> list = questionEntityPage.getRecords();
        for (QuestionEntity question : list){
            Long id = question.getCreator();
            UserEntity userEntity = userImpl.getById(id);
            question.setUser(userEntity);
        }
    }

    public void incView(Long id){
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setId(id);
        questionEntity.setViewCount(1L);
        questionMapper.incView(questionEntity);
    }


    /**
     * 根据questionEntity的id和tag来查找相关问题
     * @param questionEntity
     * @return
     */
    public List<QuestionEntity> selectRelated(QuestionEntity questionEntity) {
        String tag = questionEntity.getTag();
        if(StringUtils.isBlank(tag)){
            return new ArrayList<>();
        }
        String[] tags = StringUtils.split(tag, ",");
        String regexpTag = Arrays.stream(tags).collect(Collectors.joining("|"));
        QuestionEntity question = new QuestionEntity();
        question.setId(questionEntity.getId());
        question.setTag(regexpTag);
        List<QuestionEntity> questionEntities = questionMapper.selectRelated(question);
        return questionEntities;
    }
}
