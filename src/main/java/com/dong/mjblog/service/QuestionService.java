package com.dong.mjblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dong.mjblog.entity.QuestionEntity;

/**
 * @Author:dxf
 * @Date:2020/10/5-10-05 11:23
 */
public interface QuestionService extends IService<QuestionEntity> {
}
