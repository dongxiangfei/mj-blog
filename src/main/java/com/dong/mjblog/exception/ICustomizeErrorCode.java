package com.dong.mjblog.exception;

public interface ICustomizeErrorCode {
    String getMessage();
    Integer getCode();
}
