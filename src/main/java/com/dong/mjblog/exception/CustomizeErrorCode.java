package com.dong.mjblog.exception;

/**
 * 区分业务来封装不同的异常
 */
public enum CustomizeErrorCode implements ICustomizeErrorCode {

    QUESTION_NOT_FOUND(2001,"您找的问题不存在！"),
    NOT_LOGIN(2002,"未登录不能进行评论，请先登录"),
    TARGET_PARAM_NOT_FOUND(2003,"未选中任何问题或评论进行回复！"),
    SYSTEM_ERROR(2004,"服务器错误"),
    TYPE_PARAM_ERROR(2005,"评论类型错误或不存在"),
    COMMENT_NOT_FOUND(2006,"评论错误或不存在"),
    CONTENT_IS_EMPTY(2006,"评论内容不能为空"),
    ;

    private String message;
    private Integer code;

    CustomizeErrorCode(Integer code, String message) {
        this.message = message;
        this.code = code;
    }


    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Integer getCode() {
        return code;
    }
}
