package com.dong.mjblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dong.mjblog.entity.UserEntity;

/**
 * @Author:dxf
 * @Date:2020/10/5-10-05 11:15
 */
public interface UserMapper extends BaseMapper<UserEntity> {

}
