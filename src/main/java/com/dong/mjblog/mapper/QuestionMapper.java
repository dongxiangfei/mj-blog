package com.dong.mjblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dong.mjblog.entity.QuestionEntity;

import java.util.List;


/**
 * @Author:dxf
 * @Date:2020/10/12-10-12 23:15
 */
public interface QuestionMapper extends BaseMapper<QuestionEntity> {

    IPage<QuestionEntity> getPageList(Page<?> page);

    int incView(QuestionEntity record);

    int incCommentCount(QuestionEntity commentCount);

    List<QuestionEntity> selectRelated(QuestionEntity question);
}
