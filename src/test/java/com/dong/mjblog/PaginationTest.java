package com.dong.mjblog;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dong.mjblog.entity.QuestionEntity;
import com.dong.mjblog.mapper.QuestionMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertThat;

import javax.annotation.Resource;


@SpringBootTest
@RunWith(SpringRunner.class)
public class PaginationTest {

    @Resource
    private QuestionMapper questionMapper;

    @Test
    public void lambdaPagination(){
        Page<QuestionEntity> page = new Page<>(1, 2);
        Page<QuestionEntity> result = questionMapper.selectPage(page,null);
        System.out.println(result.getTotal());
        System.out.println(result.getRecords());
    }
}
